package id.mikaapp.mikaintentdemo

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main_menu.*

class MainMenuActivity : AppCompatActivity() {

    companion object {
        const val MIKA_INTENT_REQUEST_CODE = 10
        var lastSuccessTransactionId: String? = null
        var username: String? = null
        var password: String? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_menu)
        mainMenuTransaction.setOnClickListener {
            startActivity(Intent(this, TransactionActivity::class.java))
        }
        mainMenuSettlement.setOnClickListener {
            startActivity(Intent(this, SettlementActivity::class.java))
        }
        mainMenuVoid.setOnClickListener {
            startActivity(Intent(this, VoidActivity::class.java))
        }
        mainMenuPrint.setOnClickListener {
            startActivity(Intent(this, PrintActivity::class.java))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MIKA_INTENT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                requireNotNull(data)
                val approvalCode = data.getStringExtra("APPROVAL_CODE")
                Toast.makeText(
                    this,
                    "TRANSACTION SUCCESS\n" +
                            "Approval Code: $approvalCode",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (resultCode == Activity.RESULT_CANCELED) {
                val errorMessage = data?.getStringExtra("ERROR_MESSAGE")
                Toast.makeText(
                    this,
                    "TRANSACTION CANCELLED\n $errorMessage",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }
}
